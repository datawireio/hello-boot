FROM openjdk:8-alpine
MAINTAINER Datawire <dev@datawire.io>
LABEL PROJECT_REPO_URL         = "git@bitbucket.org:datawireio/hello-boot.git" \
      PROJECT_REPO_BROWSER_URL = "https://bitbucket.org/datawireio/hello-boot" \
      DESCRIPTION              = "Hello Boot" \
      VENDOR                   = "Datawire, Inc." \
      VENDOR_URL               = "https://datawire.io/"

ARG SRV_NAME
ENV SRV_NAME ${SRV_NAME}

ARG SRV_VERSION
ENV SRV_VERSION ${SRV_VERSION}

ARG JAR_NAME
ENV JAR_NAME ${JAR_NAME}

# Install System Dependencies

#RUN apk --no-cache add

# Set WORKDIR to /service which is the root of all our apps.
WORKDIR /service

# Install application dependencies

#RUN

# COPY the app code and configuration into place then perform any final configuration steps.
COPY target/${JAR_NAME} \
     src/main/shell/entrypoint-docker.sh \
     ./

ENTRYPOINT ["./entrypoint-docker.sh"]
