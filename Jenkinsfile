#!groovy

node {
    currentBuild.result = "SUCCESS"

    // Kubernetes Ops ("kops") state storage
    //
    // This is used to resolve the Kubernetes cluster credentials without the need for Loom which is not yet installable
    // as a standalone service.
    def kopsStateStore = "loom-state-914373874199"

    // Remote endpoint running the Docker Registry.
    def dockerRegistryHost = "https://docker.io"

    // Kubernetes cluster name in DNS form (e.g. prod.k736.net)
    def clusterName = "plombardi.k736.net"

    def service_name    = "hello-boot"
    def service_version = "0.1.0"

    def docker_img_owner = "datawire"
    def docker_img_name  = service_name
    def docker_img_final_name = ""

    def jar_base_name = service_name

    def err = null
    try {
        def jar_final_name = ""

        stage('clone') {
            git url: 'https://plombardi@bitbucket.org/datawireio/hello-boot.git', branch: 'master'
            checkout scm

            def git_commit_short = sh(returnStdout: true, script: 'git rev-parse HEAD').trim().take(6)

            jar_final_name = "${jar_base_name}.${service_version}+${BUILD_NUMBER}.${git_commit_short}.jar"
            docker_image_final_name = "${docker_img_owner}/${docker_img_name}:${service_version}-${BUILD_NUMBER}-${git_commit_short}"
        }

        stage('build with maven') {
            withMaven(maven: 'M3') {
                sh "mvn clean install"
                sh "cp ./target/${jar_base_name}-${service_version}.jar ./target/${jar_final_name}"
            }
        }

        stage('upload to artifactory') {
            // TODO: Upload the JAR to artifactory
        }

        stage('bake and push with docker') {
            withCredentials([[$class: 'UsernamePasswordMultiBinding',
                              credentialsId: 'docker-login',
                              usernameVariable: 'USERNAME',
                              passwordVariable: 'PASSWORD']]) {

                sh "docker build -t '${docker_image_final_name}' --build-arg=SRV_NAME=${service_name} --build-arg=SRV_VERSION=${service_version} --build-arg=JAR_NAME=${jar_final_name} ."
                sh "docker login -u=${env.USERNAME} -p=${env.PASSWORD}"
                sh "docker push ${docker_image_final_name}"
            }
        }

        stage('run on kubernetes') {
            // BEGIN: FOR TESTING ONLY
            //
            // This command is only used when running Jenkins in a Dockerfile on a local workstation because the EC2
            // metadata service is not present to provide credentials. It *SHOULD* be removed before production use.
            sh "ln -sf /root/.aws ${env.WORKSPACE}/.aws"
            // END: FOR TESTING ONLY

            withEnv(["HOME=${env.WORKSPACE}"]) {
                sh "kops export kubecfg --name=${clusterName} --state=s3://${kopsStateStore}"
            }

            withEnv(["KUBECONFIG=${env.WORKSPACE}/.kube/config"]) {
                sh "sed -i 's|__docker_image__|${docker_image_final_name}|g' kube/deployment.yaml".toString()
                sh 'cat kube/deployment.yaml'
                sh 'kubectl apply -f kube/ns.yaml'
                sh "kubectl apply -f kube/ --namespace=${service_name}"
            }
        }
    } catch (ex) {
        err = ex
        currentBuild.result = "FAILURE"
        throw err // propagate
    }
}
